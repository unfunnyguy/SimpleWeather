package com.exp.weather

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp class SimpleWeather : Application()
