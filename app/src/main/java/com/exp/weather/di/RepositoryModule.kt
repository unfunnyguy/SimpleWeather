package com.exp.weather.di

import com.exp.weather.data.WeatherRepositoryImpl
import com.exp.weather.domain.WeatherRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun bindsWeatherRepository(repositoryImpl: WeatherRepositoryImpl): WeatherRepository
}
