package com.exp.weather.di

import android.content.Context
import com.exp.weather.SimpleWeather
import com.exp.weather.domain.WeatherRepository
import com.exp.weather.domain.use_case.GetWeather
import com.exp.weather.domain.use_case.WeatherUseCases
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideApplication(@ApplicationContext app: Context): SimpleWeather {
        return app as SimpleWeather
    }

    @Singleton
    @Provides
    fun providesWeatherUseCases(repository: WeatherRepository): WeatherUseCases {
        return WeatherUseCases(getWeather = GetWeather(repository))
    }
}
