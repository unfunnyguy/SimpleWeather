package com.exp.weather.presentation.screens

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.exp.weather.data.remote.model.weather.Coord
import com.exp.weather.domain.use_case.WeatherUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeScreenViewModel @Inject constructor(private val useCases: WeatherUseCases) : ViewModel() {

    private var _uiState = mutableStateOf(HomeState())
    val uiState: State<HomeState> = _uiState

    //TODO: Handle Error
    fun getWeather(coord: Coord) {
        viewModelScope.launch {
            val result = useCases.getWeather(lat = coord.lat.toString(), lon = coord.lon.toString())
            result.data?.let {
                _uiState.value =
                    _uiState.value.copy(
                        weatherData = result.data,
                        isLoading = false,
                        currentCoord = coord
                    )
            }
        }
    }
}
