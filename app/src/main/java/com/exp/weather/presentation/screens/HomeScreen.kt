package com.exp.weather.presentation.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@SuppressLint("SuspiciousIndentation")
@Destination
@Composable
fun HomeScreen(viewModel: HomeScreenViewModel) {

    val state = viewModel.uiState.value

    Column(modifier = Modifier.fillMaxSize()) {
        Text(
            modifier = Modifier,
            text = state.weatherData.toString() ?: "",
            textAlign = TextAlign.Center,
            style =
                TextStyle(
                    color = MaterialTheme.colorScheme.onBackground,
                    fontSize = 25.sp,
                    fontWeight = FontWeight.SemiBold,
                )
        )

        Text(
            modifier = Modifier.padding(30.dp),
            text = state.weatherData?.name ?: "Not Found",
            textAlign = TextAlign.Center,
            style =
                TextStyle(
                    color = MaterialTheme.colorScheme.onBackground,
                    fontSize = 25.sp,
                    fontWeight = FontWeight.SemiBold,
                )
        )
    }
}
