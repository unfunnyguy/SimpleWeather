package com.exp.weather.presentation.screens

import android.Manifest
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.exp.weather.R
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@OptIn(ExperimentalPermissionsApi::class)
@Destination(start = true)
@Composable
fun LocationRequestScreen(navigator: DestinationsNavigator) {
    val permissionState =
        rememberPermissionState(
            Manifest.permission.ACCESS_COARSE_LOCATION,
        )
    if (permissionState.status.isGranted) {
        Log.d("test", "isGranted")
    }
    LocationRequestContent(
        modifier = Modifier,
        onButtonClick = { permissionState.launchPermissionRequest() }
    )
}

@Composable
private fun LocationRequestContent(modifier: Modifier, onButtonClick: () -> Unit) {
    // TODO: Everything is a placeholder
    ConstraintLayout(modifier = modifier.fillMaxSize()) {
        val (image, text, button) = createRefs()
        val leftGuideLine = createGuidelineFromStart(25.dp)
        val rightGuideLine = createGuidelineFromEnd(25.dp)

        Image(
            modifier =
                Modifier.size(125.dp).constrainAs(image) {
                    start.linkTo(leftGuideLine)
                    end.linkTo(rightGuideLine)
                    top.linkTo(parent.top, 90.dp)
                },
            painter = painterResource(R.drawable.ic_location),
            colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.primary),
            contentDescription = ""
        )

        Text(
            modifier =
                Modifier.constrainAs(text) {
                    start.linkTo(image.start)
                    end.linkTo(image.end)
                    top.linkTo(image.top, 20.dp)
                    bottom.linkTo(button.top, 55.dp)
                },
            text = "Hey please allow location permission :)",
            textAlign = TextAlign.Center,
            style =
                TextStyle(
                    color = MaterialTheme.colorScheme.onBackground,
                    fontSize = 25.sp,
                    fontWeight = FontWeight.SemiBold,
                )
        )

        Button(
            modifier =
                Modifier.constrainAs(button) {
                    start.linkTo(leftGuideLine)
                    end.linkTo(rightGuideLine)
                    bottom.linkTo(parent.bottom, 20.dp)
                },
            onClick = onButtonClick
        ) {
            Text(
                modifier = Modifier.padding(6.dp),
                text = "allow access",
                textAlign = TextAlign.Center,
                style =
                    TextStyle(
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontSize = 25.sp,
                        fontWeight = FontWeight.SemiBold,
                    )
            )
        }
    }
}
