package com.exp.weather.presentation.screens

import com.exp.weather.data.remote.model.weather.Coord
import com.exp.weather.data.remote.model.weather.NetworkWeather

data class HomeState(
    val isLoading: Boolean = true,
    val weatherData: NetworkWeather? = null,
    val currentCoord: Coord? = null
)
