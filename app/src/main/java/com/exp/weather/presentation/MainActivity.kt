package com.exp.weather.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.hilt.navigation.compose.hiltViewModel
import com.exp.weather.SimpleWeather
import com.exp.weather.data.remote.model.weather.Coord
import com.exp.weather.presentation.screens.HomeScreenViewModel
import com.exp.weather.presentation.screens.NavGraphs
import com.exp.weather.presentation.screens.destinations.HomeScreenDestination
import com.exp.weather.ui.theme.SimpleWeatherTheme
import com.google.accompanist.navigation.material.ExperimentalMaterialNavigationApi
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.android.gms.location.LocationServices
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.animations.rememberAnimatedNavHostEngine
import com.ramcosta.composedestinations.navigation.dependency
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private val viewModel: HomeScreenViewModel by viewModels()
    @SuppressLint("MissingPermission", "RememberReturnType")
    @OptIn(
        ExperimentalAnimationApi::class,
        ExperimentalMaterialNavigationApi::class,
        ExperimentalPermissionsApi::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val splashScreen = installSplashScreen()
        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        setContent {

            val state = viewModel.uiState.value
            val permissionState =
                rememberPermissionState(
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                )
            val isPermissionGranted = permissionState.status.isGranted

            //TODO: Offline Loading
            splashScreen.setKeepOnScreenCondition {
                if (isPermissionGranted) state.isLoading else false
            }

            SimpleWeatherTheme {

                if (isPermissionGranted) {
                    fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
                        location?.let {
                            viewModel.getWeather(Coord(lat = it.latitude, lon = it.longitude))
                        }
                    }
                }

                SimpleWeatherApp(isPermissionGranted, this)
            }
        }
    }
}

@OptIn(ExperimentalAnimationApi::class, ExperimentalMaterialNavigationApi::class)
@Composable
private fun SimpleWeatherApp(isPermissionGranted: Boolean, activity: MainActivity){

    val engine = rememberAnimatedNavHostEngine()
    val navController = engine.rememberNavController()
    val startRoute =
        if (isPermissionGranted) HomeScreenDestination else NavGraphs.root.startRoute

    DestinationsNavHost(
        navController = navController,
        navGraph = NavGraphs.root,
        engine = engine,
        startRoute = startRoute,
        dependenciesContainerBuilder = {
            dependency(hiltViewModel<HomeScreenViewModel>(activity))
        }
    )

}
