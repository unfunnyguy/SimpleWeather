package com.exp.weather.utils.extensions

import retrofit2.HttpException

fun Throwable.fetchError(): String {
    val unKnownException = "Something Went Wrong"
    return when (this) {
        is HttpException -> this.response()?.errorBody()?.string() ?: unKnownException
        is InternalError -> unKnownException
        else -> unKnownException
    }
}
