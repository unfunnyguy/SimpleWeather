package com.exp.weather.utils

data class ResourceState<out T>(val status: Status?, val data: T?, val errorBody: String?) {
    companion object {
        fun <T> success(data: T?): ResourceState<T> {
            return ResourceState(Status.SUCCESS, data, null)
        }
        fun <T> successBasic(): ResourceState<T> {
            return ResourceState(Status.SUCCESS_SIMPLE, null, null)
        }
        fun <T> error(errorBody: String?, data: T? = null): ResourceState<T> {
            return ResourceState(Status.ERROR, data, errorBody)
        }
        fun <T> networkError(
            errorBody: String = "Please Check your internet connection",
            data: T? = null
        ): ResourceState<T> {
            return ResourceState(Status.NETWORK_ERROR, data, errorBody)
        }
        fun <T> loading(data: T? = null): ResourceState<T> {
            return ResourceState(Status.LOADING, data, null)
        }
    }
}

enum class Status {
    SUCCESS,
    SUCCESS_SIMPLE,
    ERROR,
    NETWORK_ERROR,
    LOADING
}
