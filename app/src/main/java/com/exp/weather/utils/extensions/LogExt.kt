package com.exp.weather.utils.extensions

import android.util.Log

inline fun <reified T> T.debug(value: (T) -> String) {
    Log.d(T::class.java.simpleName, value(this))
}
