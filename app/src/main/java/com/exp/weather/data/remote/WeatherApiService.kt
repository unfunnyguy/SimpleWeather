package com.exp.weather.data.remote

import com.exp.weather.data.remote.model.weather.NetworkWeather
import com.exp.weather.utils.ApiConst.WEATHER
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiService {

    @GET(WEATHER)
    suspend fun getWeather(
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("appid")
        appid: String = "4a8abb9676de5de0052574c54abb01e4" // TODO: Move to buildconfig
    ): NetworkWeather
}
