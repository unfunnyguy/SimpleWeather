package com.exp.weather.data

import com.exp.weather.data.remote.WeatherApiService
import com.exp.weather.data.remote.model.weather.NetworkWeather
import com.exp.weather.domain.WeatherRepository
import com.exp.weather.utils.ResourceState
import com.exp.weather.utils.extensions.fetchError
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(private val apiService: WeatherApiService) :
    WeatherRepository {

    override suspend fun getWeather(lat: String, lon: String): ResourceState<NetworkWeather> {
        return try {
            val result = apiService.getWeather(lat, lon)
            ResourceState.success(result)
        } catch (throwable: Throwable) {
            throwable.printStackTrace()
            ResourceState.error(throwable.fetchError())
        }
    }
}
