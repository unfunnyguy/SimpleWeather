package com.exp.weather.data.remote.model.weather

data class Wind(val deg: Int, val gust: Double, val speed: Double)
