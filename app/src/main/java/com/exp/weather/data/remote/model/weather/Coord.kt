package com.exp.weather.data.remote.model.weather

data class Coord(val lat: Double, val lon: Double)
