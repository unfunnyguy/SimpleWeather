package com.exp.weather.domain.use_case

import com.exp.weather.data.remote.model.weather.NetworkWeather
import com.exp.weather.domain.WeatherRepository
import com.exp.weather.utils.ResourceState

class GetWeather(private val repository: WeatherRepository) {

    suspend operator fun invoke(lat: String, lon: String): ResourceState<NetworkWeather> {
        return repository.getWeather(lat, lon)
    }
}
