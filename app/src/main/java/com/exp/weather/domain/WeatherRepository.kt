package com.exp.weather.domain

import com.exp.weather.data.remote.model.weather.NetworkWeather
import com.exp.weather.utils.ResourceState

interface WeatherRepository {
    suspend fun getWeather(lat: String, lon: String): ResourceState<NetworkWeather>
}
