package com.exp.weather.domain.use_case

data class WeatherUseCases(val getWeather: GetWeather)
